namespace ContactWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MyNewConfiguration3 : DbMigration
    {
        public override void Up()
        {
            //Sql("INSERT INTO Movies (Id,Name,GenreID,ReleaseDate,DateAdded,NumInStock) " +
              //  "VALUES (1,'Hangover','1','6/15/2018','1/1/2019','10')");
            Sql("INSERT INTO Contacts (Birthday, City, Email, FirstName, LastName, PhonePrimary, PhoneSecondary, State, StreetAddress1, StreetAddress2, UserId, Zip)" +
               " VALUES ('1/20/1920','Chicago','doc.mccoy@starfleet.com','DeForest','Kelly','123-456-7890','234-567-8901','IL','Sickbay','Starship Enterprise NCC-1701','ad22a62a-68eb-47e3-9c79-a17c99fa2b47','98765')");
            /*"" +
                "Id = 1,
            Birthday = new DateTime(1920, 01, 20),
                      City = "Chicago"
                                      ,
                      Email = "doc.mccoy@starfleet.com",
                      FirstName = "DeForest",
                      LastName = "Kelley"
                                      ,
                      PhonePrimary = "123-456-7890",
                      PhoneSecondary = "234-567-8901",
                      State = "IL"
                                      ,
                      StreetAddress1 = "Sickbay",
                      StreetAddress2 = "Starship Enterprise NCC-1701"
                                      ,
                      UserId = new Guid("ad22a62a-68eb-47e3-9c79-a17c99fa2b47")
                                      ,
                      Zip = "98765"
                  }
                  , new Models.Contact
                  {
                      Id = 2,
                      Birthday = new DateTime(1920, 03, 03),
                      City = "New York"
                                      ,
                      Email = "i.beam.you.up@starfleet.com",
                      FirstName = "James",
                      LastName = "Doohan"
                                      ,
                      PhonePrimary = "345-678-9012",
                      PhoneSecondary = "456-789-0123",
                      State = "NY"
                                      ,
                      StreetAddress1 = "Engineering",
                      StreetAddress2 = "Starship Enterprise NCC-1701"
                                      ,
                      UserId = new Guid("ad22a62a-68eb-47e3-9c79-a17c99fa2b47")
                                      ,
                      Zip = "87654"
                  }
                  , new Models.Contact
                  {
                      Id = 3,
                      Birthday = new DateTime(1931, 03, 26),
                      City = "Los Angeles"
                                      ,
                      Email = "its.only.logic@starfleet.com",
                      FirstName = "Leonard",
                      LastName = "Nimoy"
                                      ,
                      PhonePrimary = "987-654-3210",
                      PhoneSecondary = "876-543-2109",
                      State = "CA"
                                      ,
                      StreetAddress1 = "Science Station 1",
                      StreetAddress2 = "Starship Enterprise NCC-1701"
                                      ,
                      UserId = new Guid("5f6f5f33-6693-41d0-80b5-c92077c0e7c2")
                                      ,
                      Zip = "76543-2109"
                  }
                  , new Models.Contact
                  {
                      Id = 4,
                      Birthday = new DateTime(1931, 03, 22),
                      City = "Riverside"
                                      ,
                      Email = "the.captain@starfleet.com",
                      FirstName = "William",
                      LastName = "Shatner"
                                      ,
                      PhonePrimary = "765-432-1098",
                      PhoneSecondary = "654-321-0987",
                      State = "IA"
                                      ,
                      StreetAddress1 = "The Bridge",
                      StreetAddress2 = "Starship Enterprise NCC-1701"
                                      ,
                      UserId = new Guid("5f6f5f33-6693-41d0-80b5-c92077c0e7c2")
                                      ,
                      Zip = "65432-0123" */          
        }
        
        public override void Down()
        {
           // DropTable("dbo.Contacts");
        }
    }
}
