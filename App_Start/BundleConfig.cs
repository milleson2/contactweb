﻿using System.Web;
using System.Web.Optimization;

namespace ContactWeb
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/datatables").Include(
                        "~/js/lib/jquery.dataTables.min.js",
                        "~/js/lib/dataTables.bootstrap.min.js",
                        "~/js/lib/dataTables.colReorder.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/css/jquery.dataTables.min.css",
                      "~/css/jquery.dataTables.min.css",
                      "~/css/datatables.bootstrap.min.css",
                      "~/css/jquery.dataTables_themeroller.css",
                      "~/css/colReorder.bootstrap.min.css",
                      "~/Content/site.css"));//"~/css/bootstrap.min.css"

            /* bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/css/jquery.dataTables.min.css",
                      "~/css/datatables.bootstrap.min.css",
                      "~/css/jquery.dataTables_themeroller.css",
                      "~/css/colReorder.bootstrap.min.css",
                      "~/css/bootstrap.min.css")); */
        }

    }
}
